﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Web.Models;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        public async Task<IActionResult> Index()
        {
            var client = new HttpClient();

            //var res = await client.GetAsync("http://localhost:19081/SFTestApp/VoteForService/api/votes");
            var res = await client.GetAsync("http://votefor.sftestapp/api/votes");
            var json = await res.Content.ReadAsStringAsync();
            var forRes = JsonConvert.DeserializeObject<VotesCountResponse>(json);

            var againstRes = await client.GetAsync("http://localhost:19081/SFTestApp/VoteAgainstService/api/votes");
            var againtsJson = await againstRes.Content.ReadAsStringAsync();
            var againstResRes = JsonConvert.DeserializeObject<VotesCountResponse>(againtsJson);

            return View(new IndexViewModel(){ForVotes = forRes.VoutesCount, AgainstVotes = againstResRes.VoutesCount});
        }

        public async Task<IActionResult> VoteFor()
        {
            var client = new HttpClient();
            //var res = await client.GetAsync("http://localhost:19081/SFTestApp/VoteForService?PartitionKey=partition1");
            var res = await client.PostAsync("http://localhost:19081/SFTestApp/VoteForService/api/votes", null);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> VoteAgainst()
        {
            var client = new HttpClient();
            var res = await client.PostAsync("http://localhost:19081/SFTestApp/VoteAgainstService/api/votes", null);

            return RedirectToAction("Index");
        }
    }
}
