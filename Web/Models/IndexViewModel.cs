﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Models
{
    public class IndexViewModel
    {
        public int ForVotes { get; set; }
        public int AgainstVotes { get; set; }
    }
}
