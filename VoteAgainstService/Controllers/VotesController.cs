﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.ServiceFabric.Data;
using Microsoft.ServiceFabric.Data.Collections;
using VoteAgainstService.Models;

namespace VoteAgainstService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VotesController : ControllerBase
    {

        public VotesController()
        {
        }

        [HttpGet]
        public async Task<VotesCountResponse> Get()
        {
            return new VotesCountResponse() {VoutesCount = 5};
        }

        [HttpPost]
        public async Task<VotesCountResponse> Post()
        {
            return new VotesCountResponse() { VoutesCount = 5};
        }
    }

}