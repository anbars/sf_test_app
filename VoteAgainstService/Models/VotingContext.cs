﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace VoteAgainstService.Models
{
    public class VotingContext : DbContext
    {
        public VotingContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<VoteRecord> VoteRecords { get; set; }
    }

    public class VoteRecord
    {
        public string Id { get; set; }
        public int Count { get; set; }
    }
}
