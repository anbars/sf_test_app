﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.ServiceFabric.Data;
using Microsoft.ServiceFabric.Data.Collections;
using VoteForService.Models;

namespace VoteForService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VotesController : ControllerBase
    {
        private IReliableStateManager _stateManager;

        public VotesController(IReliableStateManager stateManager)
        {
            this._stateManager = stateManager;
        }

        [HttpGet]
        public async Task<VotesCountResponse> Get()
        {
            var votesDictionary = await _stateManager.GetOrAddAsync<IReliableDictionary<string, int>>("counts");

            using (ITransaction tx = _stateManager.CreateTransaction())
            {
                var count = await votesDictionary.GetOrAddAsync(tx, "for", k => 0);

                return new VotesCountResponse() {VoutesCount = count};
            }
        }

        [HttpPost]
        public async Task<VotesCountResponse> Post()
        {
            var votesDictionary = await _stateManager.GetOrAddAsync<IReliableDictionary<string, int>>("counts");

            using (ITransaction tx = _stateManager.CreateTransaction())
            {
                var count = await votesDictionary.GetOrAddAsync(tx, "for", k => 0);
                await votesDictionary.SetAsync(tx, "for", ++count);
                await tx.CommitAsync();

                return new VotesCountResponse() { VoutesCount = count };
            }
        }
    }
}